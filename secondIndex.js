const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const app = express();
const port = 8000;

const password = 'password';

app.get('/:encode', (req, res) => {
    res.send(Vigenere.Cipher(password).crypt(`<div>${req.params.encode}</div>`));
});

app.get('/home/:decode', (req, res) => {
    res.send(Vigenere.Decipher(password).crypt(`<div>${req.params.decode}</div>`));
});


app.listen(port, () => {
    console.log('We are live on ' + port);
});
